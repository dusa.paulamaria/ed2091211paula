import java.time.LocalDate;

public class Person {
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		if(fechaNacimiento.isAfter(LocalDate.now())) {
			throw new IllegalArgumentException("La fecha de nacimiento no puede ser posterior a la fecha actual");
		}
		this.fechaNacimiento = fechaNacimiento;
	}
	@Override
	public String toString() {
		return getNombre() + " " + getApellidos();
	}
	
}
